
require("dotenv").config();
const request = require("supertest");
const app = require("../src/app");
const mongoose = require('mongoose')

describe("Test the root path", () => {
  test("It should response the GET method", async (done) => {
    const response = await request(app).get("/api");
    expect(response.statusCode).toBe(200);
    done();
  });

  afterAll(async () => {
    await mongoose.connection.close()
  })
});