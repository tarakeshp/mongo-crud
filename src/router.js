const express = require("express");
const UserCtrl = require("./controllers/user");

let router = express.Router();

router.get("/", function(req, res) {
    res.send("Hello World");
});

router.get("/users", async function(req, res) {
    await new UserCtrl(req, res).list();
});

router.get("/users/:id", async function(req, res) {
    await new UserCtrl(req, res).getById();
});

router.post("/users",  async function(req, res) {
    await new UserCtrl(req, res).create();
});

router.put("/users/:id",  async function(req, res) {
    await new UserCtrl(req, res).update();
});

router.delete("/users/:id", async function(req, res) {
    await new UserCtrl(req, res).delete();
});

module.exports = router;