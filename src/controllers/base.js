class BaseController {
            
    constructor(req, res, next) {
        this.req = req;
        this.res = res;
        this.next = next;
    }

    _200(data, message) {
        return this.res.status(200).json({
            status : 'success',
            data
        });
    }

    _500(message, e) {
        console.log(e);
        var db_error = (e._message && e.errors) ? e._message + "." + Object.keys(e.errors).join(",") + " are required" : e._message
        return this.res.status(400).json({
            status : 'error',
            message : db_error || message
        })
    }

    _403(message) {
        return this.res.status(403).json({
            status: 'error',
            message : message || 'No results found'
        })
    }
}

module.exports = BaseController;