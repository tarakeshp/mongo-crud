const base = require("./base");
const { UserModel } = require("../db/models");

class UserController extends base {
    constructor(req, res, next) {
        super(req, res, next);
    }

    async list() {
        const list = await UserModel.find();
        return this._200(list.map(v => v.toViewModel()));
    }

    async getById() {
        const item = await UserModel.findOne({ _id: this.req.params.id });
        if (item === null) {
            return this._403(`User with the id ${this.req.params.id} not found`);
        }
        return this._200(item.toViewModel());
    }

    async create() {
        try {
            const model = new UserModel(this.req.body);
            const result = await model.save();
            this._200({ id: result.id });
        }
        catch (e) {
            this._500(e.message, e);
        }
    }

    async update() {
        try {
            const item = await UserModel.findOne({ _id: this.req.params.id });
            if (item === null) {
                return this._403(`User with the id ${this.req.params.id} not found`);
            }
            await UserModel.findOneAndUpdate({ _id: this.req.params.id }, { $set: this.req.body });
            this._200({ id: this.req.params.id });
        }
        catch (e) {
            this._500(e.message, e);
        }
    }

    async delete() {
        try {
            const item = await UserModel.findOne({ _id: this.req.params.id });
            if (item === null) {
                return this._403(`User with the id ${this.req.params.id} not found`);
            }
            await UserModel.findOneAndDelete({ _id: this.req.params.id });
            this._200({ id: this.req.params.id });
        }
        catch (e) {
            this._500(e.message, e);
        }
    }
}

module.exports = UserController;