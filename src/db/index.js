const mongoose = require('mongoose');
console.log(process.env.MONGO_URI);
(async () => {
    try {
        await mongoose.connect(process.env.MONGO_URI, { useUnifiedTopology: true, useNewUrlParser: true, keepAlive: true, useFindAndModify: false });
        console.log("db connection success");
    }
    catch (e) {
        console.error("db connection failed", e);
        process.exit(9999);
    }
})();

module.exports = mongoose;


