const mongoose = require("mongoose");
const uuid = require("uuid");
const dayjs = require("dayjs");

/******** SCHEMA *******/
const schema = mongoose.Schema({
    _id: { type: String, default: function genUUID() { return uuid.v1() } },
    first_name: {
        type: String,
        trim: true,
        required: true,
        maxlength: 128
    },
    last_name: {
        type: String,
        trim: true,
        required: true,
        maxlength: 128
    },
    email: { type: String, required: true }
}, {
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at'
});

/******** VIRTUAL OR TRANSIENT PROPS *******/
const fullName = schema.virtual('full_name');
fullName.get(function (value, virtual, doc) {
    return this.first_name + ' ' + this.last_name;
});

/******** MODEL *******/
const model = mongoose.model('users', schema);

/******** ViewModel extension method *******/
model.prototype.toViewModel = function () {
    return {
        id: this._id,
        first_name: this.first_name,
        last_name: this.last_name,
        email : this.email,
        full_name: this.full_name,
        created_at: this.createdAt,
        updated_at : this.updatedAt
    }
}

module.exports = model;