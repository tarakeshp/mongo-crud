const express = require("express");
const router = require("./router");
const db = require("./db");
const app = new express();


app.use(express.json());
app.use("/api", router);


module.exports = app;

